package l_1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This is a simple test class which provides until tests for the House class
 * from exercise 1
 * 
 * @author css2ht
 * 
 */
class HouseTest {
	
	private House house; 
	
	@BeforeEach
	public void setUp() {
	 	house = new House();
	 	
	 	/** Set up a particular kind of house */		 
		Lounge lounge1 = new Lounge(10.0);
		DiningRoom dining1 = new DiningRoom(16.0);
	    
		house.addRoom(lounge1);
		house.addRoom(dining1);
	}
   
	/**
	 * 3. The house shall be able to display how many rooms it has. 
	 */
	@Test
	void testRequirement3() {
		assertEquals(2, house.howManyRooms());
	}
	
	/**
	 * 4. The house shall be able to display the total area of all its rooms. 
	 */;
	@Test
	void testRequirement4() {
		assertEquals(26.0, house.getTotalArea());
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		house = null;
	}
	

}
