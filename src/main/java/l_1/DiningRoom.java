package l_1;

public class DiningRoom implements Room {
 	private double area = 0.0;

	public DiningRoom(double area) {
		this.area = area;
	}


	@Override
	public double getArea() {
		return this.area;
	}

}
