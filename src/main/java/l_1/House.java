package l_1;

import java.util.ArrayList;
import java.util.List;

public class House {

	private List<Room> rooms = new ArrayList<Room>();

	public House() {

	}

	/**
	 * simple method to add a room to a house
	 * 
	 * @param room
	 */
	public void addRoom(Room room) {
		this.rooms.add(room);
	}

	public int howManyRooms() {
		return this.rooms.size();
	}

	/**
	 * Complete a foreach loop to work out the total area of the rooms in the house
	 * 
	 * @return
	 */

	public double getTotalArea() {
		double total = 0.0;
		// missing the loop....

		return total;
	}
}
