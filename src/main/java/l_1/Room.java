package l_1;


/**
 * This is a skeleton class which provides an interface
 * 
 * @author css2ht and Joe Appleton
 *
 */

public interface Room {
    
	/**
	 * Gets the area of a room.
	 * @param void. 
	 * @return double.
	 */
	public double getArea();
}
