# Lab 1

This is the source code for COM1028 lab_1

## Getting Started

1. Open Eclipse
2. When you see the open dialogue, click on WorkSpace and create a new workspace called something meaningful (e.g., "COM1028_Labs"). A workspace is simply a folder where like projects are stored. If you don't see the dialogue box, click on File -> Switch Workspace -> Other... and create a new workspace.
3. Launch the workspace.

### Importing the Lab 1 Project 

1. Click on File -> Import -> Git -> Projects from Git -> Next -> Clone URI -> Next ->
2. Enter the following into URI field: https://gitlab.surrey.ac.uk/com_1028_labs/lab_1.git
3. Click Next 
4. Import as general project -> Next -> Finish